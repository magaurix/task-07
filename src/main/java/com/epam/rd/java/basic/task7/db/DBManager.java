package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.function.Consumer;
import java.util.stream.Collectors;


public class DBManager {

	Connection connection;

	private static final String SQL_FIND_ALL_USERS = "SELECT * FROM users";
	private static final String SQL_FIND_ALL_TEAMS = "SELECT * FROM teams";
	private static final String SQL_FIND_USER_TEAMS = "SELECT teams.* FROM teams LEFT JOIN users_teams ON teams.id=users_teams.team_id LEFT JOIN users ON users_teams.user_id=users.id WHERE users.login=?";
	private static final String SQL_FIND_USER_BY_LOGIN = "SELECT * FROM users WHERE login = ?";
	private static final String SQL_FIND_TEAM_BY_NAME = "SELECT * FROM teams WHERE name = ?";
	private static final String SQL_INSERT_INTO_USERS = "INSERT INTO users(login) VALUES(?)";
	private static final String SQL_INSERT_INTO_TEAMS = "INSERT INTO teams(name) VALUES(?)";
	private static final String SQL_DELETE_FROM_TEAMS = "DELETE FROM teams WHERE id=?";
	private static final String SQL_UPDATE_TEAM = "UPDATE teams SET name=? WHERE id=?";

	private static final String TEMPLATE_DELETE_FROM_USERS = "DELETE FROM users WHERE id IN (%s)";
	private static final String TEMPLATE_INSERT_INTO_USERS_TEAM = "INSERT INTO users_teams (user_id, team_id) VALUES %s";



	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if(instance==null)instance = new DBManager();
		return instance;
	}

	private DBManager() {
		Properties props = new Properties();
		String dbSettingsPropertyFile = "app.properties";
		try {
			FileReader fReader = new FileReader(dbSettingsPropertyFile);
			props.load(fReader);
			if(props.containsKey("connection.driver")) {
				Class.forName(props.getProperty("connection.driver"));
			}
			connection = DriverManager.getConnection(props.getProperty("connection.url"));
		} catch (Exception e){
			System.out.println("Connection error "+e.getMessage());
			return;
		}
		fillDatabaseFromScript("sql"+ File.separator +"db-create.sql");
	}

	private void fillDatabaseFromScript(String filename) {
		File fileDir = new File(filename);
		try ( BufferedReader in = new BufferedReader( new InputStreamReader(new FileInputStream(fileDir), StandardCharsets.UTF_8))){
			String str;
			StringBuilder builder = new StringBuilder();
			while ((str = in.readLine()) != null) {
				builder.append(str);
				if(str.lastIndexOf(";")!=-1){
					Statement stmt = connection.createStatement();
					stmt.execute(builder.toString().trim());
					stmt.close();
					builder = new StringBuilder();
				}
			}
		} catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	public List<User> findAllUsers() throws DBException {
		try(Statement stmt = connection.createStatement();
			ResultSet rset = stmt.executeQuery(SQL_FIND_ALL_USERS))
		{
			List<User> ordersList = new LinkedList<>();
			while(rset.next())
			{
				User user = new User();
				user.setId(rset.getInt("id"));
				user.setLogin(rset.getString("login"));
				ordersList.add(user);
			}
			return ordersList;
		} catch ( SQLException e ){
			throw new DBException(e.getMessage(),e);
		}
	}

	public boolean insertUser(User user) throws DBException {
		try(PreparedStatement stmt = connection.prepareStatement(SQL_INSERT_INTO_USERS,Statement.RETURN_GENERATED_KEYS))
		{
			stmt.setString(1,user.getLogin());
			return insertToDb(stmt,user::setId);
		} catch ( SQLException e ){
			throw new DBException(e.getMessage(),e);
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		try(Statement stmt = connection.createStatement())
		{
			String ids = Arrays.stream(users).map(u -> String.valueOf(u.getId())).collect(Collectors.joining(","));
			return stmt.executeUpdate(String.format(TEMPLATE_DELETE_FROM_USERS,ids))==users.length;
		} catch ( SQLException e ){
			throw new DBException(e.getMessage(),e);
		}
	}

	public User getUser(String login) throws DBException {
		try(PreparedStatement stmt = connection.prepareStatement(SQL_FIND_USER_BY_LOGIN))
		{
			stmt.setString(1, login);
			ResultSet resultSet = stmt.executeQuery();
			User user = null;
			if(resultSet.next())
			{
				user = new User();
				user.setId(resultSet.getInt("id"));
				user.setLogin(resultSet.getString("login"));
			}
			return user;
		} catch ( SQLException e ){
			throw new DBException(e.getMessage(),e);
		}
	}

	public Team getTeam(String name) throws DBException {
		try(PreparedStatement stmt = connection.prepareStatement(SQL_FIND_TEAM_BY_NAME))
		{
			stmt.setString(1, name);
			ResultSet resultSet = stmt.executeQuery();
			Team team = null;
			if(resultSet.next())
			{
				team = new Team();
				team.setId(resultSet.getInt("id"));
				team.setName(resultSet.getString("name"));
			}
			return team;
		} catch ( SQLException e ){
			throw new DBException(e.getMessage(),e);
		}
	}

	public List<Team> findAllTeams() throws DBException {
		try(Statement stmt = connection.createStatement();
			ResultSet rset = stmt.executeQuery(SQL_FIND_ALL_TEAMS))
		{
			return getTeams(rset);
		} catch ( SQLException e ){
			throw new DBException(e.getMessage(),e);
		}
	}

	public boolean insertTeam(Team team) throws DBException {
		try(PreparedStatement stmt = connection.prepareStatement(SQL_INSERT_INTO_TEAMS,Statement.RETURN_GENERATED_KEYS))
		{
			stmt.setString(1,team.getName());
			return insertToDb(stmt,team::setId);
		} catch ( SQLException e ){
			throw new DBException(e.getMessage(),e);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		String values = Arrays.stream(teams).map(t -> "(" + user.getId() + "," + t.getId() + ")").collect(Collectors.joining(","));
		try(Statement stmt = connection.createStatement())
		{
			connection.setAutoCommit(false);
			stmt.executeUpdate(String.format(TEMPLATE_INSERT_INTO_USERS_TEAM,values));
			connection.commit();
		} catch (SQLException e){
			try {
				connection.rollback();
			} catch (SQLException ex) {
				throw new DBException(e.getMessage(),e);
			}
			throw new DBException(e.getMessage(),e);
		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				throw new DBException(e.getMessage(),e);
			}
		}
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		try(PreparedStatement stmt = connection.prepareStatement(SQL_FIND_USER_TEAMS))
		{
			stmt.setString(1,user.getLogin());
			try(ResultSet resultSet = stmt.executeQuery()) {
				return getTeams(resultSet);
			}
		} catch ( SQLException e ){
			throw new DBException(e.getMessage(),e);
		}
	}

	public boolean deleteTeam(Team team) throws DBException {
		try(PreparedStatement stmt = connection.prepareStatement(SQL_DELETE_FROM_TEAMS))
		{
			stmt.setInt(1,team.getId());
			return stmt.executeUpdate() == 1;
		} catch ( SQLException e ){
			throw new DBException(e.getMessage(),e);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		try(PreparedStatement stmt = connection.prepareStatement(SQL_UPDATE_TEAM))
		{
			stmt.setString(1, team.getName());
			stmt.setInt(2,team.getId());
			return stmt.executeUpdate() == 1;
		} catch ( SQLException e ){
			throw new DBException(e.getMessage(),e);
		}
	}

	private List<Team> getTeams(ResultSet resultSet) throws SQLException {
		List<Team> ordersList = new LinkedList<>();
		while(resultSet.next())
		{
			Team team = new Team();
			team.setId(resultSet.getInt("id"));
			team.setName(resultSet.getString("name"));
			ordersList.add(team);
		}
		return ordersList;
	}

	private boolean insertToDb(PreparedStatement stmt, Consumer<Integer> func) throws SQLException {
		boolean ok = stmt.executeUpdate() == 1;
		if(ok){
			try(ResultSet generatedKeys = stmt.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					func.accept((int)generatedKeys.getLong(1));
				} else {
					throw new SQLException("Insert failed, no ID obtained.");
				}
			}
		}
		return ok;
	}


}
